terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {
  // host = "unix:///var/run/docker.sock"
}

resource "docker_image" "ubuntu_image" {
  name = "ubuntu:latest"
}
resource "docker_container" "ubuntu_container" {
  image = "ubuntu:latest"
  name  = "my_ubuntu_container"
  command = ["/bin/sh", "-c", "while true; do sleep 1000; done"]
}
